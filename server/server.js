const express = require('express');
const path = require('path');
const fs = require('fs');

const imagesFolder = path.join(__dirname, 'static/');
const app = express();

app.use(express.static(imagesFolder));

app.get('/', (req, res) => {
    fs.readdir(imagesFolder, (err, files) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.json(files)
    });
});

app.listen(4000, () => {
    console.log('Dummie API running.');
});