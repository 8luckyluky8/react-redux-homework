import React from 'react';

import './Like.css';

const Like = ({ isLiked, toggleLike }) => (
    <div className="like">
        <i className={'heart-icon icon-heart' + (isLiked ? '' : '-empty empty')} onClick={toggleLike} title="Like" />
    </div>
);

Like.propTypes = {
    isLiked: React.PropTypes.bool.isRequired,
    toggleLike: React.PropTypes.func.isRequired
};

export default Like;