import React from 'react';

import './Delete.css';

const Delete = ({ onDelete }) => (
    <div className="delete">
        <i className="icon-trash delete-icon" onClick={onDelete} title="Delete"/>
    </div>
);

Delete.propTypes = {
    onDelete: React.PropTypes.func.isRequired
};

export default Delete;