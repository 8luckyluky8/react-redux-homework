import React from 'react';

import './LoadingScreen.css';

const LoadingScreen = () => (
    <div id="loading">
        Loading ...
    </div>
);

export default LoadingScreen;