import React from 'react';

import './Filter.css';

const Filter = ({ onFilter, placeholder, value }) => (
    <div id="filter-wrapper">
        <input id="filter" type="text" placeholder={placeholder} onChange={onFilter} value={value} />
    </div>
);

Filter.propTypes = {
    onFilter: React.PropTypes.func,
    placeholder: React.PropTypes.string,
    value: React.PropTypes.string
};

export default Filter;
