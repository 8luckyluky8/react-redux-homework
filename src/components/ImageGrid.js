import React from 'react';

import Image from '../containers/Image';

const ImageGrid = ({ images }) => (
    <div>
        {images.map(image => (
            <Image img={image} key={image.id} id={image.id} />
        ))}
    </div>
);

ImageGrid.propTypes = {
    images: React.PropTypes.array.isRequired
};

export default ImageGrid;