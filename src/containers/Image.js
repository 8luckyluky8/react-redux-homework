import React from 'react';
import { connect } from 'react-redux';

import { toggleImageLike, deleteImage } from '../actions/images';

import Like from '../components/Like';
import Delete from '../components/Delete';

import '../components/Image.css';

const Image = ({ img, toggleLike, onDelete }) => (
    <div className="image-container">
        <div className="title">{img.title}</div>
        <img className="image" src={img.src} alt={img.title} />

        <div className="controls-row">
            <Like isLiked={img.isLiked} toggleLike={toggleLike.bind(this, img.id)} />
            <Delete onDelete={onDelete.bind(this, img.id)} />
        </div>
    </div>
);

Image.propTypes = {
    toggleLike: React.PropTypes.func.isRequired,
    onDelete: React.PropTypes.func.isRequired,
    img: React.PropTypes.object.isRequired
};

export default connect(
    (state) => ({}),
    (dispatch) => ({
        toggleLike: (id) => dispatch(toggleImageLike(id)),
        onDelete: (id) => dispatch(deleteImage(id))
    }),
)(Image);