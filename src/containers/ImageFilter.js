import React from 'react';
import { connect } from 'react-redux';

import { handleFilterImages } from '../actions/filter';

import Filter from '../components/Filter';

const ImageFilter = ({ onFilter, value }) => (
    <div classID="image-filter">
        <Filter placeholder="Filter images" onFilter={onFilter} value={value} />
    </div>
);

ImageFilter.propTypes = {
    onFilter: React.PropTypes.func.isRequired,
    value: React.PropTypes.string.isRequired
};

export default connect(
    (state) => ({
        value: state.filter
    }),
    (dispatch) => ({
        onFilter: (text) => dispatch(handleFilterImages(text))
    }),
)(ImageFilter);
