import React, { Component } from 'react';
import { connect } from 'react-redux';

import LoadingScreen from '../components/LoadingScreen';
import ImageGrid from '../components/ImageGrid';
import ImageFilter from '../containers/ImageFilter';

import { handleLoadImages } from '../actions/images'

const url = 'http://localhost:4000/';

class App extends Component {
    static propTypes = {
        loading: React.PropTypes.bool.isRequired,
        loadImages: React.PropTypes.func.isRequired,
        images: React.PropTypes.array,
    };

    componentDidMount()
    {
        this.props.loadImages();
    }

    render() {
        if (this.props.loading)
        {
            return <LoadingScreen />
        }

        return (
            <div>
                <ImageFilter />
                <ImageGrid images={this.props.images.filter(image => (
                    image.isVisible
                ))} />
            </div>
        );
    }
}

export default connect(
    (state) => ({
        loading: state.loading,
        images: state.images
    }),
    (dispatch) => ({
        loadImages: () => dispatch(handleLoadImages(url))
    })
)(App);
