import { filterImages, showImages } from './images';

export const setImagesFilter = (text) => {
    return {
        type: 'SET_FILTER_IMAGES',
        text
    };
};

export const unsetImagesFilter = () => {
    return {
        type: 'UNSET_FILTER_IMAGES'
    };
};

/**
 * Middleware actions
 */

export const handleFilterImages = (event) => {
    const text = event.target.value;

    return (dispatch) => {
        if (!text.length)
        {
            dispatch(unsetImagesFilter());
            dispatch(showImages());
            return;
        }

        dispatch(setImagesFilter(text));
        dispatch(filterImages(text));
    };
};