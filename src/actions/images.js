import { setLoading } from './loading';

const addImages = (images, url) => {
    return {
        type: 'ADD_IMAGES',
        images,
        url
    };
};

export const toggleImageLike = (id) => {
    return {
        type: 'TOGGLE_IMAGE_LIKE',
        id
    };
};

export const deleteImage = (id) => {
    return {
        type: 'DELETE_IMAGE',
        id
    };
};

export const filterImages = (text) => {
    return {
        type: 'FILTER_IMAGES',
        text
    };
};

export const showImages = () => {
    return {
        type: 'SHOW_IMAGES'
    };
};

/**
 * Async actions
 */
const loadImages = async (url) => {
    try {
        const response = await fetch(url);
        const data = await response.json();

        return {
            success: 1,
            data
        };
    }
    catch(e) {
        console.log(e);
        return { success:0 };
    }
};

/**
 * Middleware actions
 */

export function handleLoadImages(url)
{
    return (dispatch) => {
        dispatch(setLoading(true));

        loadImages(url).then(result => {
            if (result.success)
            {
                dispatch(setLoading(false));
                dispatch(addImages(result.data, url));
            }
        }).catch(err => {
            console.log(err);
        });
    };
}