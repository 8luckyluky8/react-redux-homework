import { combineReducers } from 'redux';

import images from './images';
import loading from './loading';
import filter from './filter';

const app = combineReducers({
    images,
    loading,
    filter
});

export default app;