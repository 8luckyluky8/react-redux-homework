import Immutable from 'immutable';

const image = (state, action) => {
    let localState;

    switch (action.type)
    {
        case 'ADD_IMAGE':
            return {
                id: action.id,
                src: action.url + action.src,
                title: action.src.substring(0, action.src.indexOf('.')),
                isLiked: false,
                isVisible: true
            };

        case 'TOGGLE_IMAGE_LIKE':
            if (state.id !== action.id)
            {
                return state;
            }

            localState = Immutable.fromJS(state);
            return localState.set('isLiked', !localState.get('isLiked')).toJSON();

        case 'FILTER_IMAGES':
            if (state.title.indexOf(action.text) !== -1)
            {
                return state;
            }

            localState = Immutable.fromJS(state);
            return localState.set('isVisible', false).toJSON();

        case 'SHOW_IMAGES':
            localState = Immutable.fromJS(state);
            return localState.set('isVisible', true).toJSON();

        default:
            return state;
    }
};

const images = (state = [], action) => {
    let images = [];

    switch (action.type)
    {
        case 'ADD_IMAGES':
            action.images.forEach((item, index) => {
                images.push(image(undefined, {
                    type: 'ADD_IMAGE',
                    id: index,
                    src: item,
                    visible: true,
                    url: action.url
                }));
            });
            return images;

        case 'TOGGLE_IMAGE_LIKE':
            return state.map(img => image(img, action));

        case 'DELETE_IMAGE':
            return state.filter(img => img.id !== action.id);

        case 'FILTER_IMAGES':
            return state.map(img => image(img, action));

        case 'SHOW_IMAGES':
            return state.map(img => image(img, action));

        default:
            return state;
    }
};

export default images;