const filter = (state = '', action) => {
    switch (action.type)
    {
        case 'SET_FILTER_IMAGES':
            return action.text;

        case 'UNSET_FILTER_IMAGES':
            return '';

        default:
            return state;
    }
};

export default filter;